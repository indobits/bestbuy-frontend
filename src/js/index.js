
import React from 'react';
import { render } from 'react-dom';
import Buscador from './widgets/containers/search.js';

const container = document.getElementById('root');
if (container) {
  render(<Buscador />, container);
}
//console.log('oli');